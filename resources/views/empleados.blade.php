@extends('adminlte::page')
@section('title', 'Consorcio software.')
@section('content_header')
<html>
<head>
    <title>||Manejar empleados ||</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">

</head>
@section('content')
<body>
<div>
<h1>Ver  <small>empleados</small></h1>
<div class="container">
<a href="agregarempleado"> Agregar empleado </a>
</div>
<div class="container">
<table class="table-bordered" id="banca">
    <thead>
        <th>Id</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Email</th>
        <th>Numero de telefono</th>
        <th>Fecha de contrato</th>
        <th>Id puesto</th>
        <th>Salario</th>
        <th>Comision</th>
        <th>ID supervisor</th>
        <th>ID departamento</th>
        <th> - </th>

    </thead>
    <tbody>
        @foreach($empleados as $empleados)
            <tr>
                <td> {{ $empleados->ID_EMPLEADO }}</td>
                <td> {{ $empleados->NOMBRE }}</td>
                <td> {{ $empleados->APELLIDO }}</td>
                <td> {{ $empleados->EMAIL }}</td>
                <td> {{ $empleados->NUMERO_TELEFONO }}</td>
                <td> {{ $empleados->FECHA_CONTRATO }}</td>
                <td> {{ $empleados->ID_PUESTO }}</td>
                <td> {{ $empleados->SALARIO }}</td>
                <td> {{ $empleados->COMISION}}</td>
                <td> {{ $empleados->ID_SUPERVISOR }}</td>
                <td> {{ $empleados->ID_DEPARTAMENTO}}</td>
               <td>
               <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal-{{$empleados->id}}">
                        Edit
                    </button>
                    <form action="{{route('empleados.destroy',$empleados->id)}}"  method="POST">
                       {{csrf_field()}}
                       {{method_field('DELETE')}}
                       <input class="btn btn-sm btn-danger" type="submit" value="Delete">
                     </form>
              </td>
            </tr>
        @endforeach
    </tbody>
</table>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
 <script>
    $(document).ready(function(){
    $('#banca').DataTable({
    });
});
 </script>
</div>
</body>
</html>
@stop
