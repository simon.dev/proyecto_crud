@extends('adminlte::page')

@section('title', 'Consorcio software.')

@section('content_header')
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Agregando empleados</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="/css/style.css" rel="stylesheet">
  </head>

  <body>
@section('content')
 @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   <div class="container">

  <h1 class="display-3">Agregar empleado</h1>

  <form method="POST" action="empleados">
  {{ csrf_field() }}

    <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">ID_EMPLEADO</label>
    <div class="col-10">
      <input class="form-control" type="text" name="id" id="example-text-input">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">Nombre empleado</label>
    <div class="col-10">
      <input class="form-control" type="text" name="nombre" id="example-text-input">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">Apellido empleado</label>
    <div class="col-10">
      <input class="form-control" type="text" name="apellido" id="example-text-input">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">email empleado</label>
    <div class="col-10">
      <input class="form-control" type="text" name="email" id="example-text-input">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">numero de telefono</label>
    <div class="col-10">
      <input class="form-control" type="tel" name="numero_telefono" id="example-text-input">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">fecha contrato</label>
    <div class="col-10">
      <input class="form-control" type="date" name="contrato" id="example-text-input">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">id puesto</label>
    <div class="col-10">
      <input class="form-control" type="text" name="id_puesto" id="example-text-input">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">salario</label>
    <div class="col-10">
      <input class="form-control" type="text" name="salario" id="example-text-input">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">comision</label>
    <div class="col-10">
      <input class="form-control" type="text" name="comision" id="example-text-input">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">id supervisor</label>
    <div class="col-10">
      <input class="form-control" type="text" name="id_supervisor" id="example-text-input">
    </div>
  </div>

  <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">id departamento</label>
    <div class="col-10">
      <input class="form-control" type="text" name="id_departamento" id="example-text-input">
    </div>
  </div>



   <div class="form-group row">
    <label for="example-text-input" class="col-2 col-form-label">Registrar</label>
    <div class="col-10">
      <input class="btn btn-success" type="submit"  id="example-text-input">
    </div>
  </div>
  </form>


  </div>

    </div>


  </body>
</html>
@stop
