<?php

namespace App\Http\Controllers;

use App\empleados;
use Illuminate\Http\Request;

class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $empleados = EMPLEADOS::all();

        return view('empleados', compact('empleados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function agregar()
    {

      return view('agregarempleado');
    }
    public function create()
    {
        //



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $empleados = new empleados;

        /*$this->validate($request, [
            'central' => 'required|numeric|filled|max:50',
            'grupo' => 'required|digits_between:1,100',
            'nombre_banca' => 'required|string|max:30',
            'numero_banca' => 'required|numeric|max:5',
            'numero_telefono' => 'required|alpha_num|max:10',
            'direccion_banca' => 'required|string|max:50',
    ]);*/


        $empleados->ID_EMPLEADO = request('id');
        $empleados->NOMBRE = request('nombre');
        $empleados->APELLIDO = request('apellido');
        $empleados->EMAIL = request('email');
        $empleados->NUMERO_TELEFONO = request('numero_telefono');
        $empleados->FECHA_CONTRATO = request('contrato');
        $empleados->ID_PUESTO = request('id_puesto');
        $empleados->SALARIO = request('salario');
        $empleados->COMISION = request('comision');
        $empleados->ID_SUPERVISOR = request('id_supervisor');
        $empleados->ID_DEPARTAMENTO = request('id_departamento');

        $empleados->save();

        return redirect('/empleados');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function show(empleados $empleados)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function edit(empleados $empleados)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, empleados $empleados)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\empleados  $empleados
     * @return \Illuminate\Http\Response
     */
    public function destroy(empleados $empleados)
    {
        //
        DB::table("empleados")->where('id',$id)->delete();
        return back()->withMessage('Campo eliminado');
    }
}
